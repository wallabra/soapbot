package org.soapbot.futures;



interface Task {
    public function performTask(loop: TaskLoop): Void;
}