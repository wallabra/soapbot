package org.soapbot.futures;



class TaskWrapper implements Task {
    var fn: (loop: TaskLoop) -> Void;

    public function new(fn: (loop: TaskLoop) -> Void) {
        this.fn = fn;
    }

    public function performTask(loop: TaskLoop) {
        this.fn(loop);
    }
}