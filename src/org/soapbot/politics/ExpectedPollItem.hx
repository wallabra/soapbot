package org.soapbot.politics;

import org.soapbot.futures.TaskLoop;
import org.soapbot.futures.ExternalTimer;



class ExpectedPollItem implements PollItem {
    var expecting: Array<() -> Void> = new Array();
    var description: String;

    public function new(description: String) {
        this.description = description;
    }

    public function describe(): String {
        return description;
    }

    public function expects(callback: () -> Void) {
        expecting.push(callback);
    }

    public function onVoted(poll: Poll): Void {}
    public function onWin  (poll: Poll): Void {
        epi_onWin(poll);
        
        for (exp in expecting) {
            exp();
        }
    }

    function epi_onWin(poll: Poll): Void {}
}