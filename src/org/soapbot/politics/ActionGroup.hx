package org.soapbot.politics;



class ActionGroup implements Action {
    public var actionStack: Array<Action> = new Array();

    public function new() {}

    public function addAction(action: Action) {
        actionStack.push(action);
    }

    public function act() {
        for (item in actionStack) {
            item.act();
        }
    }
    
    public function undo() {
        for (i in 0...actionStack.length) {
            actionStack[actionStack.length - i].undo();
        }
    }

    public function actsOn(): Null<Actable> {
        return null;
    }

    public function describe(): String {
        return '<action group with ${actionStack.length} actions>';
    }

    public function getType(): ActionType {
        return AT_UNKNOWN;
    }
}