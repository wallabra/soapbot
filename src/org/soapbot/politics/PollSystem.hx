package org.soapbot.politics;



class PollSystem {
    var polls: Array<Poll> = new Array();
    var participants: Array<Individual> = new Array();

    public function new() {}

    public function pollOn(action: Action): Poll {
        var poll = new Poll(participants);

        var pi_yes = new ExpectedPollItem("Yes");
        var pi_no = new ExpectedPollItem("No");

        pi_yes.expects(action.act);

        poll.addItem(pi_yes);
        poll.addItem(pi_no);

        polls.push(poll);
        return poll;
    }
}