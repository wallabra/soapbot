package org.soapbot.politics;



interface SeatItem {
    public function describe(): String;
    public function length(): Int;
    public function numOccupants(): Int;
}