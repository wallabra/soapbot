package org.soapbot.politics;



class Seat implements SeatItem {
    public var occupant: Null<Individual> = null;

    public function new(occupant: Null<Individual> = null) {
        this.occupant = occupant;
    }

    public function length(): Int {
        return 1;
    }

    public function describe(): String {
        return "Individual Seat";
    }

    public function numOccupants(): Int {
        return occupant != null ? 1 : 0;
    }

    public function freeSeat() {
        if (occupant == null) return;

        occupant.onRemovedFromSeat(this);
        occupant = null;
    }

    public function appoint(to: Individual) {
        if (occupant != null) {
            this.freeSeat();
        }

        occupant = to;
        to.onAppointedToSeat(this);
    }
}