package org.soapbot.politics.actions;



class ActionOnIndividual implements Action {
    public var whom(default, null): Individual;
    public var verb: String;
    public var doWhat(default, null): (Individual) -> Void;

    public function new(whom: Individual, verb: String, doWhat: (Individual) -> Void) {
        this.whom   = whom;
        this.verb   = verb;
        this.doWhat = doWhat;
    }

    public function describe(): String {
        return '$verb "${whom.name()}"';
    }

    public function actsOn(): Null<Actable> {
        return whom;
    }

    public function getType(): ActionType {
        return AT_UNKNOWN;
    }

    public function act(): Void {
        doWhat(whom);
    }

    public function undo(): Void {
        // can't be undone...
    }
}