package org.soapbot.politics.actions;



class AddRuleAction implements Action {
    public var system(default, null): PoliticsSystem;
    public var rule  (default, null): Rule;

    public function new(system: PoliticsSystem, rule: Rule) {
        this.system = system;
        this.rule = rule;
    }

    public function describe(): String {
        return 'Add rule "${rule.describe()}"';
    }

    public function actsOn(): Null<Actable> {
        return rule;
    }

    public function getType(): ActionType {
        return AT_CREATE;
    }

    public function act(): Void {
        system.addRule(rule);
    }

    public function undo(): Void {
        system.removeRule(rule);
    }
}