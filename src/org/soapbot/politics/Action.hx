package org.soapbot.politics;



interface Action {
    public function describe(): String;

    public function getType(): ActionType;
    public function actsOn(): Null<Actable>;
    
    public function act(): Void;
    public function undo(): Void;
}