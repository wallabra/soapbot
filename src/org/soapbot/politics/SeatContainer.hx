package org.soapbot.politics;



interface SeatContainer {
    public function seatItemIterator(): Iterator<SeatItem>;
    public function addSeatItem(newSeat: SeatItem): Bool;
    public function removeSeatItem(oldSeat: SeatItem): Bool;
}