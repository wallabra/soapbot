package org.soapbot.politics;



interface PollItem {
    public function describe(): String;

    public function onWin  (poll: Poll): Void;
    public function onVoted(poll: Poll): Void;
}