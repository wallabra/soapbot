# Soapbot

is a Discord bot written in Haxe, that manages political aspects of a Discord server,
such as regular polls for moderators and legislators. Its main purpose is to assist in
developing free, democratic, unbiased, and pleasant social communities and groups.

## Details

Soapbot uses a concept where moderators apply the rules to other people, where usually
actions require an internal vote between moderators in order to be enacted (or not);
and legislators, who write and vote on new rules (or modifying and revoking existing
ones). Most people can vote on who occupies Seats for being moderators or legislators.

The role of Soapbot is to manage the seats, assigning appropriate roles, to mediate
public polls, and to help enqueue internal votes about moderator and legislator actions.